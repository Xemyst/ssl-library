from Geometry import Geometry
from dataSocket import DataSocket
from proto import messages_robocup_ssl_wrapper_pb2
#read Data from ssl-vision (https://github.com/RoboCup-SSL/ssl-vision)


class VisionData(object):
    """docstring for VisionData"""
    def __init__(self):
        ip = '224.5.23.2'
        port = 10006
        self.sock = DataSocket(grup = ip, port = port)
        self.inPacket = messages_robocup_ssl_wrapper_pb2.SSL_WrapperPacket()
        #self.initField()

    def run(self):
        #Escutar
        data = self.sock.read()
        wrapper = self.inPacket.FromString(data)
        while not wrapper.HasField('geometry'):
            data = self.sock.read()
            wrapper = self.inPacket.FromString(data)
        print(wrapper.geometry)
        if (wrapper.HasField('geometry')):
            self.Geometry.updateData(wrapper.geometry)
        if (wrapper.HasField('detection')):
            pass
        #normalizar (protobufer) https://developers.google.com/protocol-buffers/docs/pythontutorial
        #actualizar

    def initField(self):
        data = self.sock.read()
        wrapper = self.inPacket.FromString(data)
        while not wrapper.HasField('geometry'):
            data = self.sock.read()
            wrapper = self.inPacket.FromString(data)
        self.Geometry = Geometry(wrapper.geometry)

    def getData(self):
        return self.data


    def getBlueData(self):
        blue_robots = []
        data = self.sock.read()
        wrapper = self.inPacket.FromString(data)
        while not wrapper.HasField('detection'):
            data = self.sock.read()
            wrapper = self.inPacket.FromString(data)
        data = wrapper.detection.robots_blue
        for x in data:
            blue_robots += [[x.robot_id,x.x,x.y,x.orientation  ]]
        return blue_robots

    def getYellowData(self):
        yellow_robots = []
        data = self.sock.read()
        wrapper = self.inPacket.FromString(data)
        while not wrapper.HasField('detection'):
            data = self.sock.read()
            wrapper = self.inPacket.FromString(data)
        data = wrapper.detection.robots_yellow
        for x in data:
            yellow_robots += [[x.robot_id,x.x,x.y,x.orientation  ]]
        return yellow_robots

    def getBallData(self):
        data = self.sock.read()
        wrapper = self.inPacket.FromString(data)
        while not wrapper.HasField('detection'):
            data = self.sock.read()
            wrapper = self.inPacket.FromString(data)
        ball = wrapper.detection.balls
        print(len(ball))
        while len(ball) == 0 or not wrapper.HasField('detection'):
            data = self.sock.read()
            wrapper = self.inPacket.FromString(data)
            ball = wrapper.detection.balls
            print(len(ball))
        return ball[0].x , ball[0].y
