from proto import messages_robocup_ssl_wrapper_pb2

class Geometry(object):
    """docstring for Geometry. this class will contain the data of the field"""
    def __init__(self,data):
        self.field = data.field

    def updateData(self, data):
        self.field = data.field


    def getFieldSize(self):
        return (self.field.field_length, self.field.field_width)
