import socket
import struct
MCAST_GRP = 0
MCAST_PORT = 0

class DataSocket(object):
    """docstring for """
    def __init__(self, grup = MCAST_GRP, port=  MCAST_PORT):
        self.MCAST_GRP = grup
        self.MCAST_PORT = port
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.sock.bind((grup, port))
        mreq = struct.pack("4sl", socket.inet_aton(grup), socket.INADDR_ANY)
        self.sock.setsockopt(socket.IPPROTO_IP, socket.IP_ADD_MEMBERSHIP, mreq)

    def read(self):
            return  self.sock.recv(1024)
